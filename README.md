# Mesh Projects for ["Pajarito Models"](https://gitlab.com/lanl-em)

"Mesh Projects" is a ["Pajarito Models"](https://gitlab.com/lanl-em) module.

All modules under "Pajarito Models" are open-source released under GNU GENERAL PUBLIC LICENSE Version 3.

LANL Copyright Number: C18077
Copyright (c) 2018, Los Alamos National Security, LLC.
All rights reserved.

## Directories
Each directory contains a set of compressed archives containing all files used to create a mesh.


### RegionalWaffle2016

3D large-scale Regional Aquifer
(2016 V2 Smooth Waffle)


### RDXVA2016

RDX Vadose Zone for TA-16 
(2016 V1 Mesh A 40m, no wells)


### CrVA2016

3D Site-scale Chromium Vadose zone/ Regional Aquifer Mesh 
(2016 Stacked Mesh Prototype)


### EVDATA

These are EarthVision Geologic Framework Model grids written in file formats readable by LaGriT for the meshing work. 
The EarthVision model package is provided by Weston Solutions and is NOT included.


## Software Used (not included)

The numerical grids and model setup were developed using these software:

* LaGriT (lagrit.lanl.gov) Los Alamos Grid Generation Toolbox (LaGriT) software
    
* Dynamic Graphics, Inc. 2004. EarthVision 7.5: Software for 3-D Modeling and Visualization. Alameda, CA.



