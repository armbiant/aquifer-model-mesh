V2 Smooth Waffle 2016 for Regional Aquifer Model
web pages https://meshing.lanl.gov/proj/ER_LANL_Monitoring/LGM3D_Cr_wells_2016/mesh.html
and copied locally into web_pages directory

========================================================================
About this repo:

Summary of sizes with total at end:
     du -ch > RegionalWaffle2016_dirs.txt
Summary list of every file in the archive: 
     du -La > RegionalWaffle2016_files.txt

Arhive of directories and files:

 1.6G Jun 22 13:26 FEHM_DIR.tar.gz
 6.2G Jun 22 13:47 MESH_DIRS.tar.gz

Use this to extract directory from compressed tar file:
  tar -zxvf [name].tar.gz 

This was used to archive and compress directories:
  tar -zhcvf FEHM_DIR.tar.gz  FEHM_stack_omr_wells_v2
  tar -zhcvf MESH_DIRS.tar.gz stack_omr_wells_125m_v2 stack_omr_wells_125m well_screens_2016

========================================================================
FEHM MESH STATS

number of nodes =      766283         
number of elements =  4659062    
number nodes per layer = 17416
total layers = 44 (prism converted to tets)

Each layer is created from a quad mesh with 125x125 meter edges.
Refined in areas of wells to min edge length of 31.25 and then
well points are added and triangulated with some edge lengths smaller than 20 meters.
Vertical resolution is 6 meters for top 54 meters, then proportional with 42m at flat bottom.

Top is smooth version of watertable a2014 
Bottom is flat at 1000 meters
West  boundary is 489975 
North boundary is 543000
SW boundary is the LANL boundary
East boundary is the Rio Grande River


 NAME  MIN              MAX                DIFFERENCE
 xic   4.899750000E+05  5.101000000E+05    2.012500000E+04
 yic   5.267500000E+05  5.430000000E+05    1.625000000E+04
 zic   1.000000000E+03  1.924672718E+03    9.246727184E+02

Start with V1 then modify for pumping
V1 mesh /scratch/er/tam/er_monitoring_2016/grids/stack_omr_wells_125m
V2 mesh modified to avoid neg ccoefs grids/stack_omr_wells_125m_v2
The V2 workflow involved many iterations and changes to make this mesh
such that neg ccoefs are reduced or elminated.
See RUNS and README.txt in each directory

Source Input data:

Watertable surface
/scratch/er/lanl_ev_models/weston_WC15c/data/wtr_a2014_m.inp

Well data
# Waffle smooth top from mysql 28 Jun 2016
# Cr wells aproximate are CrEX-2 CrEX-3 CrIN(1-6) 
# well screen coordinates from screens.fix.dat with screen zones GRP-ID-tags-NUM
/scratch/er/tam/er_well_data/mysql_data/screen_zones_m_ft_28jun2016.dat
copied to well_screen_m_ft.dat


========================================================================
2016 Stacked Mesh and workflow


Start with V1 and V2 of 2016
Add refinement but stay under 1 Mil nodes.
Try the following:
  - Vertical stack at 3m within 120x120x30m box
  - coarsen to 250m 500m from wells
  - refine a) inside shape around focus wells
  - refine b) 100m around each focus well
  - refine c) refine again 30m around each focus well

Focus Wells:
R-50
R-28
R-42
PZ-1
PZ-2
CrIN-1-6
CrEX-1-4

========================================================================

Originally designed to be automated.
The 2D tri surface is custom
The connect into tet step is custom and subjective to neg ccoef
The generalized portion of the workflow (from V1)

# in directory well_screens, process well data into lagrit files
  ln -s well_screens/well_list.txt well_list.txt
  ln -s well_screens/wells_toppts.inp wells_toppts.inp
  ln -s well_screens/wells_allpts.inp wells_allpts.inp

# link to boundary shapes
ln -s /scratch/er/tam/er_data_objects/model_bndry_tri.gmv model_bndry_tri.gmv


# Find level 0 and vertical spacing for smooth version connect with 0 neg ccoefs
#   make quad mesh level 0 mesh and interpolate elevations
#   horizontal resolution chosen to allow 6m spacing in vertical trans from wtrtable

build_quad_surf.lgi has most the user parameters set.
stack.lgi are the vertical surfaces into hex and tet mesh

UNREFINED 3D STACK MESH
Script to create quad surface and connect into tet mesh:
  ./run_to_tet.scr

Script calls the following:
lagrit < build_quad_surf.lgi
lagrit < stack_test.lgi or stack.lgi (final)
./run_pyramid.scr
lagrit < ck_pyramid.lgi

REFINED 3D STACK MESH

First build octree plus wells in  2D_tri_surf
   uses files in well_screens
   lagrit < surf_refine.lgi
   lagrit < surf_to_tri.lgi 

ln -s 2D_tri_surf/tri_wells_full.inp tri_wells.inp

   ./run_to_tet_wells.scr
   
   calls the following:
     lagrit < stack_wells.lgi
     ./run_pyramid.scr
     lagrit < ck_pyramid_wells.lgi


============================================================================
Custom and subjective connect into tets

Stack and connect level 0 to find workable vertical resolution.
The octree refined template will interpolate elevations from coarse spacing
on the hope this will help connect to behave as well as the stacked coarse surfaces
Old version started with 125m level 0 spacing

125m with 8m vertical connects
125m with 6m has some issues
125m 14462 nodes same extents 
120m 15552 nodes modfied extents
100m 22463 nodes same extents


Constraints:
FEHM tet mesh with number of nodes under 1 Mill
Delaunay mesh with no negative Voronoi volumes
Delaunay mesh with no negative Coupling Coefficients
Vertical resolution 6m or less from top to depth of 50m
Horizontal point distribution does not move (xy well locations)
Preserve top elevations (interpolated from water table surface)

----------------------------------------------------------
V1 Final Mesh with 125m watertable elev and neg ccoef away from wells.
Pumping model major bad effects at location of neg ccoef.
/scratch/er/tam/er_monitoring_2016/grids/stack_omr_wells_125m

----------------------------------------------------------
V2 Final Mesh with 1000m watertable elev and 0 neg ccoef.
Watertable elevations within few meters except R-36 8m deviation.
Alterations at local area in mesh all fail as neg ccoefs perterb
to a new location. This is best can do within constraints.
Adding resolution along slopes may fix this, but add many nodes. 
/scratch/er/tam/er_monitoring_2016/grids/stack_omr_wells_125m_v2

----------------------------------------------------------
WORK directory
/scratch/er/tam/er_monitoring_2016/grids/work

This is the mesh with accurate watertable elevations,
not used for modeling because of neg ccoefs.


stacked prism mesh at zero coords
-rw-r--r-- 1 tamiller sft 115070952 Jul 25 08:31 pri_stack_zero.gmv
-rw-r--r-- 1 tamiller sft 186058878 Jul 25 08:31 pri_stack_zero.inp

connected tet mesh at zero coords
-rw-r--r-- 1 tamiller sft 253607646 Jul 25 08:34 tet_pyramid0.inp

connected tet mesh at real coords
-rw-r--r-- 1 tamiller sft 207438548 Jul 25 08:41 tet_wells_pyramid.gmv
-rw-r--r-- 1 tamiller sft 525090257 Jul 25 08:42 tet_wells_pyramid.inp
-rw-r--r-- 1 tamiller sft 112536964 Jul 25 08:42 tet_wells_pyr_surfmesh.gmv
-rw-r--r-- 1 tamiller sft  11194168 Jul 25 08:43 tet_wells_top_surfmesh.gmv


SparseMatrix initialize epsilon to 1.000000e-08
SparseMatrix using Epsilon 1.000000e-08
AMatbld3d_stor: *****Negative Coefficients ******                               
AMatbld3d_stor: Total Number of Negative Coefficients   123                 
AMatbld3d_stor: Number of Significant Negative Coefs    123                  
AMatbld3d_stor: Number of 'zero' (< 10e-8 *max) coefs     8     
AMatbld3d_stor: npoints =   766304  ncoefs =   10824170                         
AMatbld3d_stor: Number of unique coefs =   5795237                              
AMatbld3d_stor: Maximum num. connections to a node =     21                 
AMatbld3d_stor: Volume min =   2.4664229E+03                                    
AMatbld3d_stor: Volume max =   7.0903507E+05                                    
AMatbld3d_stor: Total Volume:   1.7187458E+11                                   
AMatbld3d_stor: abs(Aij/xij) min =   0.0000000E+00                              
AMatbld3d_stor: abs(Aij/xij) max =   2.6056667E+03                              
AMatbld3d_stor: (Aij/xij) max =   7.4160624E+00                                 
AMatbld3d_stor: (Aij/xij) min =  -2.6056667E+03                    




