WC15c EarthVision Surfaces for LaGriT

These are files converted from EarthVision geologic framework model surfaces
into file formats that can be read by LaGriT (lagrit.lanl.gov) software for the mesh work.
The source EarthVision model package is NOT included.

weston_WC15c_data     copied from  /scratch/er/lanl_ev_models/weston_WC15c/data 
weston_WC15c_grds2avs copied from  /scratch/er/lanl_ev_models/weston_WC15c/EV/surf_from_2grds 

Summary file lists for directories and files:
  du -ch EVDATA > EVDATA_dirs.txt
  du -La  EVDATA > EVDATA_files.txt

Compressed archive:
  tar -zhcvf EVDATA_DIR.tar.gz  * 

Extract with command:
  tar -zxvf EVDATA_DIR.tar.gz 

2.7G EVDATA_DIR.tar.gz



