3D Site-scale Chromium Vadose zone/ Regional Aquifer Mesh 
2016 Stacked Mesh Prototype

web pages file:///scratch/sft_temp/tam/EM/CrVA2016/web_pages/index.html 
and copied locally into web_pages directory

========================================================================
About this repo:

Summary of directory sizes:
     du -ch CrVA2016 > CrVA2016_dirs.txt
Summary list of every file in the archive: 
     du -La CrVA2016 > CrVA2016_files.txt

Arhive of directories and files:

 545M  FEHM_DIR.tar.gz
 613M  MESH_DIRS.tar.gz

Use this to extract directory from compressed tar file:
  tar -zxvf [name].tar.gz 

This was used to archive and compress directories:
  tar -zhcvf FEHM_DIR.tar.gz  fehm_stack_wc15c_proto 
  tar -zhcvf MESH_DIRS.tar.gz stack_proto_wc15c 


========================================================================
FEHM MESH STATS

Instead of large octree mesh, create stacked mesh with as few nodes as possible. 
Capture smooth layer at top of the basalts (Tb4 and Tvt2) and the watertable top.

The area of this mesh is the mininum possible with 30m quad spacing.
Clipping boundary includes the canyons LA, Sandia, Mortandad.
East boundary at R-13, west at x=497200 (includes nose of TVT2)
Variable z spacing is mostly 10m, but needed 6m to capture Tpf3 holes above basalts. 

Zones include a reduction zone at basalt interfaces and hydraulic windows

nodes      185982
elements  1130252

xmin    4.971800000E+05  xmax    5.001800000E+05
ymin    5.383800000E+05  ymax    5.406600000E+05
zmin    1.675000000E+03  zmax    2.177036657E+03


Mat Zone       Name        Nodes     #nodes/nodes  Percent 
    26          QBT          388       0.002086    0.21
    23         QBT3         1330       0.007151    0.72
    22         QBT2         2415       0.012985    1.30
    21       QBT1vu         4801       0.025814    2.58
    20       QBT1vc         2669       0.014351    1.44
    19        QBT1g         6845       0.036805    3.68
    17          QCT         4048       0.021766    2.18
    16         QBOF        26844       0.144337   14.43
    15         QBOG         2951       0.015867    1.59
    14         TPF3        10453       0.056204    5.62
    13          TB4        41977       0.225705   22.57
    11         TVT2         3019       0.016233    1.62
    10         TPF2        26311       0.141471   14.15
     7         TJFP        17699       0.095165    9.52
     5          TB2          366       0.001968    0.20
     4         TCAR        33866       0.182093   18.21
Total nodes:  185982

Source Weston-Cole (WC) Earthvision Geologic Framework Model WC15c
Locally at /scratch/er/lanl_ev_models/weston_WC15c/EV/WC15c.seq
Watertable: /scratch/er/lanl_ev_models/weston_WC15c/data/wtr_a2014_m.inp



