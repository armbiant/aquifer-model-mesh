Cr 3D Stacked Mesh Prototype using stacked mesh method.
3D Site-scale Chromium Vadose zone/ regional aquifer model
March 17 2016 tamiller@lanl.gov
Mesh work: /scratch/er/tam/cr/grids/stack_proto_wc15c
FEHM zone files: /scratch/er/tam/cr/fehm_stack_wc15c_proto/zones

-rw-r--r-- 1 tamiller sft      59545113 Mar 17 14:04 tet.fehmn
-rw-r--r-- 1 tamiller sft       2046573 Mar 17 14:04 tet_material.zone
-rw-r--r-- 1 tamiller sft       4140274 Mar 17 14:04 tet_interface.zone
-rw-r--r-- 1 tamiller sft       7667744 Mar 17  2016 tet_multi_mat.zone
-rw-r--r-- 1 tamiller sft      77483967 Mar 17  2016 tet.stor
-rw-r--r-- 1 tamiller sft      54366848 Mar 17  2016 tet_binary.stor


Need LA Canyon, smooth basalts with Tb4/Tvt2 interface 
capture Tpf3 on top of basalts with holes at R-45 and R-28
thicker in the north-west, pinchout south of R-61 (1.5m)

Sources:
Geologic Framework Model: 
  /scratch/er/lanl_ev_models/weston_WC15c/EV/WC15c.seq
  NAD83 / TM New Mexico SP, Central Zone (3002), US feet
Watertable: 
  /scratch/er/lanl_ev_models/weston_WC15c/data/wtr_a2014_m.inp

These are the primary layers for Cr 3D stacked mesh. 
  Topo - intersects all the layers above and including Qbog
  Top of Tb4/Tvt2/Tpf2 (represented by one surface)
  Tjfp or Watertable (they intersect, use watertable )
  bottom at flat 1675 (-100m from lowest point of watertable)

The area of this mesh is the mininum possible.
Clipping boundary includes canyons LA, Sandia, Mortandad
North-east LA canyon clipped to avoid basalt near topo surf.
Clip east at R-13
Clip west at x=497200 which includes nose of TVT2
30m quad spacing, variable z with min 6m at basalts

number of nodes =     185982        
number of elements = 1130252

xmin    4.971800000E+05  xmax    5.001800000E+05  
ymin    5.383800000E+05  ymax    5.406600000E+05  
zmin    1.675000000E+03  zmax    2.177036657E+03  

Mat Zone       Name        Nodes     #nodes/nodes  Percent 
    26          QBT          388       0.002086    0.21
    23         QBT3         1330       0.007151    0.72
    22         QBT2         2415       0.012985    1.30
    21       QBT1vu         4801       0.025814    2.58
    20       QBT1vc         2669       0.014351    1.44
    19        QBT1g         6845       0.036805    3.68
    17          QCT         4048       0.021766    2.18
    16         QBOF        26844       0.144337   14.43
    15         QBOG         2951       0.015867    1.59
    14         TPF3        10453       0.056204    5.62
    13          TB4        41977       0.225705   22.57
    11         TVT2         3019       0.016233    1.62
    10         TPF2        26311       0.141471   14.15
     7         TJFP        17699       0.095165    9.52
     5          TB2          366       0.001968    0.20
     4         TCAR        33866       0.182093   18.21
Total nodes:  185982


--------------------------------------------                                    
elements with aspect ratio < .01:                59248                          
elements with aspect ratio b/w .01 and .02:       7188                          
elements with aspect ratio b/w .02 and .05:       9243                          
elements with aspect ratio b/w .05 and .1 :       6152                          
elements with aspect ratio b/w .1  and .2 :       5361                          
elements with aspect ratio b/w .2  and .5 :     316048                          
elements with aspect ratio b/w .5  and 1. :     727012                          
min aspect ratio =  0.1287E-07  max aspect ratio =  0.9999E+00                  
---------------------------------------                                         
element volumes b/w  0.5664E-02 and  0.1041E+00:       861                      
element volumes b/w  0.1041E+00 and  0.1913E+01:     12577                      
element volumes b/w  0.1913E+01 and  0.3517E+02:     43876                      
element volumes b/w  0.3517E+02 and  0.6464E+03:     26820                      
element volumes b/w  0.6464E+03 and  0.1188E+05:   1046118                      
min volume =   5.6640000E-03  max volume =   1.1879798E+04                      
-----------------------------------------------------------                     


AMatbld3d_stor: *****Negative Coefficients ******                               
AMatbld3d_stor: Total Number of Negative Coefficients       742 
Negative ccoefs confirmed located on the outside top of mesh.
AMatbld3d_stor: npoints =   185982  ncoefs =    2710620                         
AMatbld3d_stor: Number of unique coefs =   1448301                              
AMatbld3d_stor: Maximum num. connections to a node =         22                 
AMatbld3d_stor: Volume min =   8.2119713E+02                                    
AMatbld3d_stor: Volume max =   4.1094357E+04                                    
AMatbld3d_stor: Total Volume:   2.6891120E+09                                   
AMatbld3d_stor: abs(Aij/xij) min =   0.0000000E+00                              
AMatbld3d_stor: abs(Aij/xij) max =   1.7350440E+02                              
AMatbld3d_stor: (Aij/xij) max =   2.6158032E+00                                 
AMatbld3d_stor: (Aij/xij) min =  -1.7350440E+02              

FEHM FILES ----------------------------------------------

-rw-r--r-- 1 tamiller sft 59545113 Mar 17 14:04 tet.fehmn
-rw-r--r-- 1 tamiller sft  2046573 Mar 17 14:04 tet_material.zone
-rw-r--r-- 1 tamiller sft  4140274 Mar 17 14:04 tet_interface.zone
-rw-r--r-- 1 tamiller sft  7667744 Mar 17 14:05 tet_multi_mat.zone
-rw-r--r-- 1 tamiller sft 77483967 Mar 17 14:06 tet.stor
-rw-r--r-- 1 tamiller sft 54366848 Mar 17 14:06 tet_binary.stor


ZONES ---------------------------------------------------

OUTSIDE by face directions 

==> out_top.zonn <== (includes multiple vert nodes at stair-steps)
zonn 000001     top nnum 7378

==> out_bottom.zonn <==
zonn 000002     bottom nnum 7154

==> out_west_left.zonn <==
zonn 000003     west nnum 1794

==> out_south.zonn <==
zonn 000004     south nnum 2522

==> out_east_right.zonn <==
zonn 000005     east nnum 1482

==> out_north_back.zonn <==
zonn 000006     north nnum 3046

==> out_north_west.zonn <== (stair-step edge between west and north zones)
zonn 000007     north_west nnum 208

==> out_south_west.zonn <== (stair-step edge between west and south zones)
zonn 000008     south_west nnum 234


WATERTABLE by surface and outside directions

==> wtr_abv.zonn <== (all nodes above watertable)
zonn 000040     abvh2o nnum 135904

==> wtr_blw.zonn <== (all nodes below watertable)
zonn 000041     blwh2o nnum 50078

==> wtr_blw_bot.zonn <== (outside bottom below watertable)
zonn 000042     blw_bot nnum 7154

==> wtr_blw_west.zonn <== (outside west below watertable)
zonn 000043     blw_west nnum 483

==> wtr_blw_south.zonn <== (outside south below watertable)
zonn 000044     blw_south nnum 679

==> wtr_blw_east.zonn <== (outside east below watertable)
zonn 000045     blw_east nnum 399

==> wtr_blw_north.zonn <== (outside north below watertable)
zonn 000046     blw_north nnum 826

==> wtr_blw_nw.zonn <== (outside north-west below watertable)
zonn 000047     blw_nw nnum 56

==> wtr_blw_sw.zonn <== (outside south-west below watertable)
zonn 000048     blw_sw nnum 63


TOP CANYON by polygon shapes
Zone middle near MCOI-4
Zone center for LA Canyon 498530.0  540270.0

==> TOP_ply_mortandad.zonn <== (Mortandad Canyon)
zonn 000062     ply_mort nnum 88

==> TOP_ply_sandia_w.zonn <== (Sandia Canyon West)
zonn 000063     ply_sandia_w nnum 36

==> TOP_ply_lac.zonn <== (LA Canyon)
zonn 000064     TOP_ply_lac nnum 52

not used ==> TOP_ply_sandia.zonn <== (Old Sandia near R-11)
   zonn 000061     ply_sandia nnum 115


Scenario 1 INTERFACE for reduction (inside and outside basalts)
Fixed to detach basalt nodes from connections to watertable.
They should be above, but grid is too coarse for good seperation.
See images.

==> basalt_in.zonn  <== zonn 000075   ba_in nnum 44918
==> basalt_out.zonn <== zonn 000076   ba_out nnum 141064

These zonn files are the mat 11 and 13 nodes reset to be outside basalt.
They are between the low point of basalt Tvt2 and the watertable.
They may be used to set these material 11 and 13 with different properties.

==> basalt_out_m11.zonn <== zonn 000111   m11_baout nnum 71
==> basalt_out_m13.zonn <== zonn 000113   m13_baout nnum 7 


# REPLACED:
# ==> basalt_in.zonn <== (all nodes in zones 11 and 13)
# zonn 000075     ba_in nnum 44996
# ==> basalt_out.zonn <== (all nodes not in basalts)
# zonn 000076     ba_out nnum 140986


Scenario 2 INTERFACE for reduction (above and below basalt top surface)

==> basalt_top_abv.zonn <== (nodes above the top Tb4/Tvt2/Tpf3 surface)
zonn 000072     bas_abv nnum 64364

==> basalt_top_eblw.zonn <== (nodes equal and below Tb4/Tvt2/Tpf3 surface)
zonn 000071     bas_eblw nnum 121618

==> basalt_top_eq.zonn <== (nodes equal to the Tb4/Tvt2/Tpf3 surface)
zonn 000070     bas_eq nnum 7154

# ---- Dan's Ellipse Window ZONES --------------------------------- #

Potential source zones at the top of the regional aquifer from Dan's Regional model. 
For exploring how these source zones may relate to hydraulic windows in the 
perched horizons above the regional aquifer. 
Each window is shaped by an ellipse provided by Dan and intersected with the 
reduction zone Scenario 1 inside the basalts. 

S1: Window in basalt and near R-42
==> basalt_win1.zonn <== zonn 000081     ba_win1 nnum 416

S2: Window in basalt and near R-62
==> basalt_win2.zonn <== zonn 000082     ba_win2 nnum 1204

S3: Window in basalt and near R-43
==> basalt_win3.zonn <== zonn 000083     ba_win3 nnum 266

Ellipse coordinates:

s1x0: 499180.3173531408
s1y0: 539062.4243310302
s1rx: 150.43645577142325
s1ry: 93.86366446350226
s1corr: -0.007755050329442792

s2x0: 498449.6660849053
s2y0: 539269.4908869121
s2rx: 224.66240483589047
s2ry: 214.44893463968026
s2corr: -0.03540951814094584

s3x0: 499034.4142389633
s3y0: 539381.9021147755
s3rx: 99.88875410175581
s3ry: 103.40419776213663
s3corr: -0.0009468194723087908


---------------------------------------------------------

