TA-16 Mesh Polygon A V1 40m with no well refinement

number of nodes =       60516        
number of elements =   345050

--------------------------------------------                                    
elements with aspect ratio < .01:                11501                          
elements with aspect ratio b/w .01 and .02:       1611                          
elements with aspect ratio b/w .02 and .05:       1362                          
elements with aspect ratio b/w .05 and .1 :        620                          
elements with aspect ratio b/w .1  and .2 :       6832                          
elements with aspect ratio b/w .2  and .5 :     216568                          
elements with aspect ratio b/w .5  and 1. :     106556                          
min aspect ratio =  0.2774E-06  max aspect ratio =  0.9759E+00                  
---------------------------------------                                         
element volumes b/w  0.8047E-02 and  0.1482E+00:        48                      
element volumes b/w  0.1482E+00 and  0.2729E+01:       612                      
element volumes b/w  0.2729E+01 and  0.5026E+02:      8504                      
element volumes b/w  0.5026E+02 and  0.9256E+03:      5815                      
element volumes b/w  0.9256E+03 and  0.1705E+05:    330071                      
min volume =   8.0469333E-03  max volume =   1.7046876E+04                      
-----------------------------------------------------------   

This mesh has negative ccoefs located only on the top outside nodes.

*** Construct and Compress Sparse Matrix:3D ***                                 
   *** Compress Area Coefficient Values ***                                     
AMatbld3d_stor: Matrix compress_eps:  0.1000000E-07                             
AMatbld3d_stor: Local epsilon:  0.1000000E-14                                   
AMatbld3d_stor: *****Negative Coefficients ******                               
AMatbld3d_stor: Total Number of Negative Coefficients        99                 
AMatbld3d_stor: Number of Significant Negative Coefs        99     
AMatbld3d_stor: npoints =    60516  ncoefs =     887280                         
AMatbld3d_stor: Number of unique coefs =    263969                              
AMatbld3d_stor: Maximum num. connections to a node =         22                 
AMatbld3d_stor: Volume min =   1.8025757E+03                                    
AMatbld3d_stor: Volume max =   8.7190155E+04                                    
AMatbld3d_stor: Total Volume:   1.2840455E+09                                   
AMatbld3d_stor: abs(Aij/xij) min =   2.6991441E-06                              
AMatbld3d_stor: abs(Aij/xij) max =   4.4924881E+02                              
AMatbld3d_stor: (Aij/xij) max =   8.6852337E-02                                 
AMatbld3d_stor: (Aij/xij) min =  -4.4924881E+02                                 
AMatbld3d_stor Matrix coefficient values stored as scalar area/distance         
AMatbld3d_stor Matrix compression used for graph and coefficient values         
ascii STOR file written with name tet.stor                              

ZONE MATERIALS ----------------------------------------------------------

File: tet_ev_material.zone
Using WC15c/TA-16.seq

Material Qbt4     25 has       761 nodes. #nodes/nnodes is   0.125751867890E-01 
Material Qbt3t    24 has      1006 nodes. #nodes/nnodes is   0.166237019002E-01 
Material Qbt3     23 has      2061 nodes. #nodes/nnodes is   0.340571105480E-01 
Material Qbt2     22 has      1747 nodes. #nodes/nnodes is   0.288683976978E-01 
Material Qbt1vu   21 has       650 nodes. #nodes/nnodes is   0.107409609482E-01 
Material Qbt1g    19 has       813 nodes. #nodes/nnodes is   0.134344631806E-01 
Material Qct      17 has      1818 nodes. #nodes/nnodes is   0.300416424870E-01 
Material Qbof     16 has      7380 nodes. #nodes/nnodes is   0.121951222420     
Material Qbof_G3  28 has      4438 nodes. #nodes/nnodes is   0.733359754086E-01 
Material Qbof_G2  27 has      4418 nodes. #nodes/nnodes is   0.730054825544E-01 
Material Tpf3     14 has     26705 nodes. #nodes/nnodes is   0.441288262606     
Material Tvt2     11 has       144 nodes. #nodes/nnodes is   0.237953593023E-02 
Material Tpf2     10 has      8575 nodes. #nodes/nnodes is   0.141698062420     

ZONN OUTSIDE ----------------------------------------------------------

Files:
out_top.zonn
out_bottom.zonn
out_east_right.zonn
out_north_back.zonn
out_north_east.zonn
out_south_front.zonn
out_west_left.zonn

1 THE PSET  top     HAS       1476 POINTS                                           
2 THE PSET  bottom  HAS       1476 POINTS                                        
3 THE PSET  west    HAS        738 POINTS                                          
4 THE PSET  south   HAS       3485 POINTS                                         
5 THE PSET  east    HAS        287 POINTS                                          
6 THE PSET  north   HAS       2501 POINTS                                         
7 THE PSET  north_east  HAS   1476 POINTS    


OUTSIDE and under WATERTABLE ZONES -------------------------------------

This mesh has a smooth water table top.
Using weston_WC15c/data/wtr_a2014_m.inp from Monty
/scratch/er/lanl_ev_models/weston_WC15c/EV/a2014.2grd

Files:
wtr_abv.zonn       <== zonn 000040     abvh2o nnum 45756
wtr_blw.zonn       <== zonn 000041     blwh2o nnum 14760
wtr_blw_bot.zonn   <== zonn 000042     blw_bot nnum 1476 
wtr_blw_west.zonn  <== zonn 000043     blw_west nnum 180
wtr_blw_south.zonn <== zonn 000044     blw_south nnum 850
wtr_blw_east.zonn  <== zonn 000045     blw_east nnum 70
wtr_blw_north.zonn <== zonn 000046     blw_north nnum 610
wtr_blw_ne.zonn    <== zonn 000047     blw_ne nnum 360

INFIL CANYON ZONE -----------------------------------------------------

Using polygon from David Broxton
This is the polygon for the alluvial groundwater infiltration zone you requested for Canon de Valle. 
The polygon extends from Pajarito fault eastwards to the end of alluvial saturation (~1200 ft east of MDA P). 
 
Note that there are well data to constrain the presence of alluvial saturation only in the eastern half of the polygon. Alluvial saturation is assumed in the western half of the polygon, but there are no well data to confirm this.  Also, the eastern extent of alluvial saturation probably extends as far east as the confluence of Canon de Valle with Water Canyon during spring runoff, but this is ephemeral and probably outside the domain of your models.
 
canyon_bot.zonn <== zonn 000102     canyon nnum 200


PERCHED WATER ZONES ----------------------------------------------------

Using surfaces from David Broxton and generated by Dan Strobridge
/scratch/er/lanl_ev_models/weston_WC15c/EV/Upper.Top.4.2grd
/scratch/er/lanl_ev_models/weston_WC15c/EV/Upper.Bottom.4.2grd
/scratch/er/lanl_ev_models/weston_WC15c/EV/Upper.Top.2.2grd
/scratch/er/lanl_ev_models/weston_WC15c/EV/Upper.Bottom.2.2grd
/scratch/er/lanl_ev_models/weston_WC15c/EV/Lower.Top.1.2grd
/scratch/er/lanl_ev_models/weston_WC15c/EV/Lower.Bottom.1.2grd

Upper Perched zone 1 (alternate smaller)
wtr_UPZ1_blw.zonn <== zonn 000060     UPZ1_blw nnum 33543
wtr_UPZ1_in.zonn  <== zonn 000061     UPZ1_in nnum 8195

Upper Perched zone 2 (alternate larger)
wtr_UPZ2_blw.zonn <== zonn 000060     UPZ2_blw nnum 33231
wtr_UPZ2_in.zonn  <== zonn 000061     UPZ2_in nnum 7566

Lower Perched zone 1
wtr_LPZ1_blw.zonn <== zonn 000070     LPZ1_blw nnum 18585
wtr_LPZ1_in.zonn  <== zonn 000071     LPZ1_in nnum 242 

!!! MODIFIED March 16 2016 !!!
Lower Perched zone has 5 nodes that connect to elements that have nodes in watertable.
Move thesee 5 nodes from zonn 71 to zonn 70 (see image)
Nodes: 16508   16580   16581   16582   16657

wtr_LPZ1_blw.zonn <== zonn 000070     LPZ1_blw nnum 18590
wtr_LPZ1_in.zonn  <== zonn 000071     LPZ1_in nnum 237

!!! MODIFIED March 30 2016 !!!
Expand UPZ2 zone so that it reaches the north-west boundary
because saturation has a dry spot northern area when infil is west only
The prototype has ymax at 538220 and has UPZ2 along the full NW edge

Modified NW Upper Perched zone 2
wtr_UPZ2_blw_addNW.zonn <== zonn 000060   UPZ2_blw nnum 33097
wtr_UPZ2_in_addNW.zonn  <== zonn 000061   UPZ2_in nnum 7700

Modified NW Upper Perched zone 2 to avoid large top elements
And to have at least 2 nodes thickness at the extension
wtr_UPZ2_blw_addNW2.zonn <== zonn 000060     UPZ2_blw nnum 33048
wtr_UPZ2_in_addNW2.zonn  <== zonn 000061     UPZ2_in nnum 7743

NEW UPZ2 ZONES for Saturated Tpf3 Material 14 = Tpf3s Material 140

Tpf3s (Tpf3 in UPZ2)
wtr_UPZ2_Tpf3s.zonn      <== zonn 000140     UPZ2_m14 nnum 2907

Tpf3s in window and not interface
wtr_UPZ2_Tpf3s_win.zonn  <== zonn 000141     TPF3s_win nnum 45

Tpf3s at interface at bottom of UPZ2
wtr_UPZ2_Tpf3s_intr.zonn <== zonn 000142     TPF3s_intr nnum 9



QUALITY Numbers ------------------------------------------------------

These are the quality checks for full Delaunay mesh (non-truncated)
There are no negative ccoefs, but many flat or nearly flat elements of small volume.
Voronoi volumes are all positive and look reasonable.
Proto mesh has simlar numbers.
Quality may be improved with higher horizontal resolution.
See /scratch/nts/tam/er/TA16/hex_polyA_V1_40m/connect/pyramid/ck_stor.lgi

AMatbld3d_stor: *****Zero Negative Coefficients ******
AMatbld3d_stor: npoints =    66420  ncoefs =     978644
AMatbld3d_stor: Number of unique coefs =    287231
AMatbld3d_stor: Maximum num. connections to a node =         22
AMatbld3d_stor: Volume min =   1.8025757E+03
AMatbld3d_stor: Volume max =   1.0507781E+05
AMatbld3d_stor: Total Volume:   1.5828480E+09
AMatbld3d_stor: abs(Aij/xij) min =   2.6991441E-06
AMatbld3d_stor: abs(Aij/xij) max =   4.4924881E+02
AMatbld3d_stor: (Aij/xij) max =  -2.6991441E-06
AMatbld3d_stor: (Aij/xij) min =  -4.4924881E+02

element volumes b/w  0.8047E-02 and  0.1677E+00:        48
element volumes b/w  0.1677E+00 and  0.3496E+01:       877
element volumes b/w  0.3496E+01 and  0.7288E+02:     11125
element volumes b/w  0.7288E+02 and  0.1519E+04:     10063
element volumes b/w  0.1519E+04 and  0.3167E+05:    360366
min volume =   8.0469333E-03  max volume =   3.1666141E+04

 THE ELTSET e_sliver                         HAS      19866 ELEMENTS
 THE ELTSET e_cap                            HAS          0 ELEMENTS
 THE ELTSET e_needle                         HAS        733 ELEMENTS
 THE ELTSET e_wedge                          HAS        992 ELEMENTS

 tet_volume         8.046933302E-03  3.166614056E+04 3.166613251E+04    382479
 vorvol             1.802575728E+03  1.050778057E+05 1.032752299E+05     66420
 ang_mind           4.322321312E-05  6.946928590E+01 6.946924268E+01    382479
 ang_minr           7.543873822E-07  1.212467768E+00 1.212467014E+00    382479
 ang_maxd           7.213874589E+01  1.799999389E+02 1.078611930E+02    382479
 ang_maxr           1.259058634E+00  3.141591587E+00 1.882532953E+00    382479
 s_mind             2.529730505E-05  3.146438013E+01 3.146435483E+01    382479
 s_minr             4.415212650E-07  5.491570303E-01 5.491565888E-01    382479
 s_maxd             2.531416936E-05  9.316856248E+01 9.316853716E+01    382479
 s_maxr             4.418156028E-07  1.626098175E+00 1.626097733E+00    382479
 aratio             8.001575700E-07  9.994059005E-01 9.994051003E-01    382479
 eratio             6.273181070E-02  9.811627258E-01 9.184309151E-01    382479
 edgemin            3.550275687E+00  5.665640574E+01 5.310613005E+01    382479
 edgemax            5.656854249E+01  8.473513400E+01 2.816659150E+01    382479


